<?php get_header();?>

<section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h3><?php the_archive_title();?></h3>
              <h4><?php the_archive_description(); ?></h4>
              <hr class="hr-name">
            </div>
            
            <div class="blog__post">
              <div class="row">
                    <?php
                        while(have_posts()){
                        the_post();?>
                                        
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 post">
                      
                      <div class="blog__post__cover">
                        <div class="blog__post__cover-picture">
                          <div class="post-date post-date-arcive">
                            <span><?php the_date('d')?></span>
                            <p><?php echo get_the_date('M')?></p>
                          </div>
                        </div>
                        <div class="blog__post__cover-header">
                          <a href="<?php the_permalink();?>"><?php the_title();?></a>
                        </div>
                        <p><?php the_content();?></p>
                        <hr>
                      </div>
                    </div>

                    <?php }?>
                    <h4><?php echo paginate_links(); ?></h4>
              </div>
            </div>
        </div>
      </section>

      <?php get_footer(); ?>