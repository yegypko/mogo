<?php 

function mogo_theme_files() {
    wp_enqueue_style('custom-google-fonts-kaushan', 'https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap');
    wp_enqueue_style('custom-google-fonts-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat&display=swap');
    wp_enqueue_style('custom-google-fonts-roboto', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
    wp_enqueue_style('mogo-slick_styles', get_theme_file_uri('/css/slick.css'), NULL, microtime());
    wp_enqueue_style('mogo-main_styles', get_stylesheet_uri(), NULL, microtime());

    wp_enqueue_script('mogo-jquery-js', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
    wp_enqueue_script('mogo-popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_enqueue_script('mogo-bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
    wp_enqueue_script('mogo-slick-js', get_theme_file_uri('/js/slick.min.js'), NULL, '1.0', true);
    wp_enqueue_script('mogo-main-js', get_theme_file_uri('/js/script.js'), NULL, microtime(), true);
}

add_action('wp_enqueue_scripts', 'mogo_theme_files');

function mogo_features() {
    add_theme_support( 'custom-logo' );
    add_theme_support('title-tag');
    add_image_size('PostItemImage', 350, 240, true);
    add_image_size('ServiceImageMain', 540, 360, true);
    add_image_size('TeamImageMain', 350, 433, true);
    add_image_size('SliderImageMain', 1920, 1000, true);
}

add_action('after_setup_theme', 'mogo_features');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}


   






