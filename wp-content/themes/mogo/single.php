<?php
get_header();?>
<?php while(have_posts()){ the_post();?>
<section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h4><?php the_title();?></h4>
              <hr class="hr-name">
            </div>
            
            <div class="blog__post">
              <div class="row">
                                        
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 post">
                      
                      <div class="blog__post__cover">
                        <div class="blog__post__cover-picture">
                          <div class="post-date">
                            <span><?php the_date('d')?></span>
                            <p><?php echo get_the_date('M')?></p>
                          </div>
                          <img src="<?php $PostImage = get_field('post_thumb'); echo $PostImage['sizes']['PostItemImage']?>" alt="cover image"/>
                        </div>
                        
                        <div class="blog__post__cover-header">
                        </div>
                        <p><?php the_content();?></p>
                        <hr>
                        <div class="blog__post__cover-contact">
                          <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>542</span></a>
                          <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>17</span></a>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
        </div>
      </section>

<?php }?>
<?php get_footer();?>