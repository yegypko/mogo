<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head();?>
  </head>
  <body>
    <header <?php body_class(); ?>>
        <div class="container">
            <nav class="menu navbar navbar-expand-lg ">
                <?php if ( function_exists( 'the_custom_logo' ) ) {the_custom_logo();} ?>
                <div class="menu__content collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo site_url('/blog')?>">Blog<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo site_url('#service')?>">Service</a>
                    </li>
                    <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('#team')?>">Team</a>
                    </li>
                    <li class="cart nav-item">
                        <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('icons/SHOPPING_CART.svg') ?>" alt="cart"></a>
                    </li>
                    <li class="serch nav-item">
                        <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('icons/MAGNIFYING_GLASS.svg') ?>" alt="search"></a>   
                    </li>
                  </ul>
                </div>
            </nav>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img src="<?php echo get_theme_file_uri('icons/menu-button.svg') ?>" alt=""></span>
            </button>
        </div>    
      </header>