<?php
get_header();?>
 <section class="team">
        <div class="container">
          <div class="section-name">
            <h3><?php the_field('title_team', 'option'); ?></h3>
            <h4><?php the_field('subtitle_team', 'option'); ?></h4>
            <hr class="hr-name">
            <p><?php the_field('about_team', 'option'); ?></p>
          <div class="row">
          <?php 
                                
              
                        while(have_posts()){
                            the_post('team');?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team__cover">
                  
                  <div class="team__cover-hover">
                    <img class="team__cover-item" src="<?php $TeamImage = get_field('team_image'); echo $TeamImage['sizes']['TeamImageMain']?>" alt="team">
                
                      <ul class="team__cover-item-social">
                        <li><a href="<?php the_field('facebook_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                        <li><a href="<?php the_field('twitter_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                        <li><a href="<?php the_field('pinterest_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                        <li><a href="<?php the_field('instagram_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                      </ul>
                    
                  </div>
                  
                  <h6><?php the_title();?></h6>
                  <span><?php the_field('social_job');?></span>

                </div>
                <?php }?>

                
          </div>
        </div>
      </section>
      
<?php get_footer();?>