$(document).ready(function(){
    $('.slider__hero').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 577,
                    settings: {
                        dots: false,
                    }
                }
            ]
    });

  });