<?php get_header(); ?>
<section class="slider">
                  <div class="slider__hero">
                  <?php 
				
                  $frontpageSlider = new WP_Query (array(
                      'posts_per_page'=> 4,
                      'post_type'   => 'slider',
                  ));
                          while($frontpageSlider->have_posts()){
                              $frontpageSlider->the_post('slider');?>

                    <div class="slider__hero__inner">
                      <div class="inner">
                        <H2 class="inner-intro"><?php the_field('slider_title_2');?></H2>
                        <h1><?php the_field('slider_content');?></h1>
                        <hr>
                        <a href="<?php the_field('slider_button_link');?>"><button><?php the_field('slider_button');?></button></a>
                        
                      </div>
                      <img src="<?php $SliderImage = get_field('slider_image'); echo $SliderImage['sizes']['SliderImageMain']?>" alt="slider-1">
                    </div>
                    <?php }wp_reset_postdata();?>
                  </div>
                  <div class="container slider__titles">
                  <?php 
                  $frontpageSlider = new WP_Query (array(
                      'posts_per_page'=> 4,
                      'post_type'   => 'slider',
                  ));
                          while($frontpageSlider->have_posts()){
                              $frontpageSlider->the_post('slider');?>
                    <div class="slider__titles__block"><p><span><?php the_field('slider_number');?></span><?php the_title();?></p></div>
                    <?php }wp_reset_postdata();?>
                  </div>
              
      </section>
      
      <section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h3><?php the_field('title_post', 'option'); ?></h3>
              <h4><?php the_field('subtitle_post', 'option'); ?></h4>
              <hr class="hr-name">
            </div>
            
            <div class="blog__post">
              <div class="row">
                  <?php 
            
                    $frontpagePosts = new WP_Query (array(
                                'posts_per_page'=> 3,
                                'post_type'   => 'post',
                                'order' => 'DESC',
                    ));
                                    while($frontpagePosts->have_posts()){
                                        $frontpagePosts->the_post('post');?>
                                        
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 post">
                      
                      <div class="blog__post__cover">
                        <div class="blog__post__cover-picture">
                          <div class="post-date">
                            <span><?php the_date('d')?></span>
                            <p><?php echo get_the_date('M')?></p>
                          </div>
                          <img src="<?php $PostImage = get_field('post_thumb'); echo $PostImage['sizes']['PostItemImage']?>" alt="cover image"/>
                        </div>
                        
                        <div class="blog__post__cover-header">
                          <a href="<?php the_permalink();?>"><?php the_title();?></a>
                        </div>
                        <p><?php echo wp_trim_words(get_the_content(), '12');?></p>
                        <hr>
                        <div class="blog__post__cover-contact">
                          <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>542</span></a>
                          <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>17</span></a>
                        </div>
                      </div>
                    </div>
                    
                    <?php }wp_reset_postdata();?>

                    <?php 
            
                    $Posts = new WP_Query (array(
                                'posts_per_page'=> -1,
                                'post_type'   => 'post',
                    ));
                        while($Posts->have_posts()){
                            $Posts->the_post('post');?>

                  <?php }wp_reset_postdata();?>
                    <?php
                          if ( $Posts->post_count > 3) {
                              ?> <div class="container all-posts"><a href="<?php echo get_post_type_archive_link('post') ?>" >View All Posts</a></div><?php
                          } 
                    ?>
                    
              </div>
            </div>
        </div>
      </section>

      <section class="service">
        <div class="container">
        <?php 
				
				$frontpageServices = new WP_Query (array(
                    'posts_per_page'=> -1,
                    'post_type'   => 'services',
				));
                        while($frontpageServices->have_posts()){
                            $frontpageServices->the_post('services');?>

            <div class="section-name">
            <h3><?php the_title();?></h3>
            <h4><?php the_field('services_title');?></h4>
            <hr class="hr-name">
            <p><?php the_field('services_content');?></p>
            </div>

            <div class="service__cover">
              <div class="row">
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div class="service__cover-img">
                      <img src="<?php $ServiceImage = get_field('services_image'); echo $ServiceImage['sizes']['ServiceImageMain']?>"/>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div id="service-accordion">
                      <div class="service__cover-card">
                        <div class="card-header" id="service-heading-4" data-toggle="collapse" data-target="#service-collapse-4" aria-expanded="true" aria-controls="service-collapse-4">
                           <img src="<?php echo get_theme_file_uri('icons/PICTURE.svg') ?>"/>
                           <?php the_field('services_title_1');?>
                        </div>
                        <div id="service-collapse-4" class="collapse show" aria-labelledby="service-heading-4" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_1');?>
                          </div>
                        </div>
                      </div>
                      <div class="service__cover-card">
                        <div class="card-header" id="service-heading-5" data-toggle="collapse" data-target="#service-collapse-5" aria-expanded="false" aria-controls="service-collapse-5">
                          <img src="<?php echo get_theme_file_uri('icons/EQUALIZER.svg') ?>"/>
                          <?php the_field('services_title_2');?>
                        </div>
                        <div id="service-collapse-5" class="collapse" aria-labelledby="service-heading-5" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_2');?>
                          </div>
                        </div>
                      </div>
                      <div class="service__cover-card">
                        <div class="card-header" id="service-heading-6" data-toggle="collapse" data-target="#service-collapse-6" aria-expanded="false" aria-controls="service-collapse-6">
                          <img src="<?php echo get_theme_file_uri('icons/BULLSEYE.svg') ?>"/>
                          <?php the_field('services_title_3');?>
                        </div>
                        <div id="service-collapse-6" class="collapse" aria-labelledby="service-heading-6" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_3');?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
           <?php }wp_reset_postdata();?>
        </div>
      </section>
      
      <section class="team">
        <div class="container">
          <div class="section-name">
            <h3><?php the_field('title_team', 'option'); ?></h3>
            <h4><?php the_field('subtitle_team', 'option'); ?></h4>
            <hr class="hr-name">
            <p><?php the_field('about_team', 'option'); ?></p>
          <div class="row">
          <?php 
                                
                $frontpageTeam = new WP_Query (array(
                    'posts_per_page'=> 3,
                    'post_type'   => 'team',
                ));
                        while($frontpageTeam->have_posts()){
                            $frontpageTeam->the_post('team');?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team__cover">
                  
                  <div class="team__cover-hover">
                    <img class="team__cover-item" src="<?php $TeamImage = get_field('team_image'); echo $TeamImage['sizes']['TeamImageMain']?>" alt="team">
                
                      <ul class="team__cover-item-social">
                        <li><a href="<?php the_field('facebook_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                        <li><a href="<?php the_field('twitter_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                        <li><a href="<?php the_field('pinterest_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                        <li><a href="<?php the_field('instagram_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                      </ul>
                    
                  </div>
                  <a href="<?php the_permalink();?>"><h6><?php the_title();?></h6></a>
                  <span><?php the_field('social_job');?></span>

                </div>
                <?php }wp_reset_postdata();?>


                <?php 
                    $Team = new WP_Query (array(
                                'posts_per_page'=> -1,
                                'post_type'   => 'team',
                    ));
                        while($Team->have_posts()){
                            $Team->the_post('team'); }
                          if ( $Team->post_count > 3) {
                              ?> <div class="container all-posts"><a href="<?php echo get_post_type_archive_link('team') ?>" >View All Team Posts</a></div><?php
                          } 
                    ?>
 
          </div>
        </div>
      </section>
    
      <?php get_footer(); ?>