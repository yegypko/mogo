<?php
get_header();?>
 <section class="team">
        <div class="container">
          <div class="section-name">
            <h3><?php the_archive_title();?></h3>
            <hr class="hr-name">
            <p><?php the_archive_description(); ?></p>
          <div class="row">
          <?php 
                                
              
                        while(have_posts()){
                            the_post('team');?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team__cover">
                  
                  <div class="team__cover-hover">
                    <img class="team__cover-item" src="<?php $TeamImage = get_field('team_image'); echo $TeamImage['sizes']['TeamImageMain']?>" alt="team">
                
                      <ul class="team__cover-item-social">
                        <li><a href="<?php the_field('facebook_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                        <li><a href="<?php the_field('twitter_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                        <li><a href="<?php the_field('pinterest_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                        <li><a href="<?php the_field('instagram_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                      </ul>
                    
                  </div>
                  
                  <a href="<?php the_permalink();?>"><h6><?php the_title();?></h6></a>
                  <span><?php the_field('social_job');?></span>

                </div>
                <?php }?>
                <h4><?php echo paginate_links(); ?></h4>
                
          </div>
        </div>
      </section>
      
<?php get_footer();?>