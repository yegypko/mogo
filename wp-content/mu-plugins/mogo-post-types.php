<?php
/**
	 * Registers a new post type
	 * @uses $wp_post_types Inserts new post type object into the list
	 *
	 * @param string  Post type key, must not exceed 20 characters
	 * @param array|string  See optional args description above.
	 * @return object|WP_Error the registered post type object, or an error object
	 */

	
	function mogo_post_types() {
		

		// Post Slider
		$labels = array(
			'name'               => __( 'Slider', 'slider' ),
			'singular_name'      => __( 'Slider', 'slider' ),
			'add_new'            => _x( 'Add New Slider', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Slider', 'slider' ),
			'edit_item'          => __( 'Edit Slider', 'slider' ),
			'new_item'           => __( 'New Slider', 'slider' ),
			'view_item'          => __( 'View Slider', 'slider' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Slider', 'slider' ),
			'all_items'			=> 'All Sliders',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-image-flip-horizontal',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'show_in_rest'		=> true,
			'supports'            => array(
				'title',
				'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'slider', $args );

		// Post Services
		$labels = array(
			'name'               => __( 'Services', 'services' ),
			'singular_name'      => __( 'Services', 'services' ),
			'add_new'            => _x( 'Add New Services', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Services', 'services' ),
			'edit_item'          => __( 'Edit Services', 'services' ),
			'new_item'           => __( 'New Services', 'services' ),
			'view_item'          => __( 'View Services', 'services' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Services', 'services' ),
			'all_items'			=> 'All Services',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-smiley',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'show_in_rest'		=> true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'services', $args );


		// Team
		$labels = array(
			'name'               => __( 'Team', 'team' ),
			'singular_name'      => __( 'Team', 'team' ),
			'add_new'            => _x( 'Add New Team', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Team', 'team' ),
			'edit_item'          => __( 'Edit GTeam', 'team' ),
			'new_item'           => __( 'New Team', 'team' ),
			'view_item'          => __( 'View Team', 'team' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Team', 'team' ),
			'all_items'			=> 'All Team',
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => true,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-admin-users',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'show_in_rest'		=> true,
			'supports'            => array(
				'title',
				'editor',
				// 'author',
				// 'thumbnail',
				'excerpt',
				
			)
			
		);
	
		register_post_type( 'team', $args );


	}
	
	add_action( 'init', 'mogo_post_types' );

